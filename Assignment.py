import json
from threading import Lock, Thread

class CoffeeMachine:

    #ServeError has all the error message for server and refill functionality
   
    class ServeError:
        INVALID_BEVERAGE = 'INVALID_BEVERAGE'
        INSUFFICIENT_INGREDIENTS = 'INSUFFICIENT_INGREDIENTS'
        MAX_LIMIT_REACHED = 'MAX_LIMIT_REACHED'
        NOT_AVAILABLE = 'NOT_AVAILABLE'


    lock = Lock()
    
    #Here in constructor we have intialize all the class varialbe 
    #outletInUse - Tells us how many outlet currenly in use
    #noOfOutlate - How many outlets are in coffeeMachine
    #ingredients - Dictionary of all the ingredients availabe in coffee machine
    #beverages - Dictionary of all the beverages which coffee machine can make

    def __init__(self, data):
        self.outletInUse = 0
        self.noOfOutlate = data["machine"]["outlets"]["count_n"]
        self.ingredients = {}
        self.beverages = {}

        for ingredients_name in data["machine"]["total_items_quantity"]:
            val = Ingredients(ingredients_name, data["machine"]["total_items_quantity"][ingredients_name])
            self.ingredients[ingredients_name] = val

        for beverages_name in data["machine"]["beverages"]:
            ingredients = []
            for ingredients_name in data["machine"]["beverages"][beverages_name]:
                val = Ingredients(ingredients_name, data["machine"]["beverages"][beverages_name][ingredients_name])
                ingredients.append(val)
            val = Bevrages(ingredients, beverages_name)
            self.beverages[beverages_name] = val

    # Refil will take input ingredients_name and Quantity, If that ingredients is present in the it quantity will increased by given quantity

    def refil(self, ingredients_name, quantity):
        self.lock.acquire()
        if ingredients_name in self.ingredients:
            self.ingredients[ingredients_name].quantity = self.ingredients[ingredients_name].quantity + quantity
        else:
            val = Ingredients(ingredients_name, quantity)
            self.ingredients[ingredients_name] = val
        self.lock.release()
    #_getbeveragedetails - take input beverage_name and gives us details of all the ingredients and quantity which we need to make this beverage
    def _getbeveragedetails(self, beverage_name):
        if beverage_name in self.beverages:
            return self.beverages[beverage_name]
        return None
    
    #_canpreparebevrage - check if the ingredient availabe in coffee machine can enough to make this given bevrage or not.
    def _canpreparebevrage(self, bevrage_details):  
        ingredientRequired = []
        notAvailable = []
        for ingredient in bevrage_details.ingredients:
            if ingredient.name not in self.ingredients:
                notAvailable.append(ingredient.name)
            else:
                if ingredient.quantity > self.ingredients[ingredient.name].quantity:
                    ingredientRequired.append(ingredient.name)
        return ingredientRequired, notAvailable
        

    #_preparebevrage - This function substract the ingredient quantity of given bevrage from availabe ingredient in coffee machine
    
    def _preparebevrage(self, bevrage_details):
        for ingredient in bevrage_details.ingredients:
            self.ingredients[ingredient.name].quantity = self.ingredients[ingredient.name].quantity - ingredient.quantity

    # Server will take input beverage_name and return if we can make that beverage or not.
    def serve(self, beverage_name):
        #print(beverage_name)
        
        self.lock.acquire()
        error = None
        if self.outletInUse < self.noOfOutlate:
            self.outletInUse = self.outletInUse + 1
            beverage_details = self._getbeveragedetails(beverage_name)   
            if beverage_details is None:
                error = self.ServeError.INVALID_BEVERAGE
                print(f"{beverage_name} can't be prepared as it is not supported")
            else: 
                itemRequired, itemNotAvailable = self._canpreparebevrage(beverage_details)
                if itemNotAvailable:
                    error = self.ServeError.NOT_AVAILABLE
                    print(f"{beverage_name} can't be prepared because {itemNotAvailable} is not Available")
                else:
                    if itemRequired:
                        error = self.ServeError.INSUFFICIENT_INGREDIENTS
                        print(f"{beverage_name} can't be prepared because item {itemRequired} is insufficient")
                    else:
                        self._preparebevrage(beverage_details)
                        print(f"{beverage_name} is prepared")
                    
                    
            self.outletInUse = self.outletInUse - 1
        else:
            error = self.ServeError.MAX_LIMIT_REACHED
            print(f"{beverage_name} can't be prepared as all outlets are occupied")
        self.lock.release()
        return error

class Ingredients:
    def __init__(self, name, quantity):
        self.name = name
        self.quantity = quantity

class Bevrages:
    def __init__(self, ingredients, name):
        self.name = name
        self.ingredients = ingredients


def __init__():
    #print("ZCsacb")
    with open('input.json', 'r') as file:
        data = json.load(file)
    
    
    a = CoffeeMachine(data)
    #a.serve("hot_tea")
    input = open('input.txt', 'r')
    Lines = input.readlines()

    for line in Lines:
        s = line.split()
        if len(s) == 1:
            a.serve(s[0])
            
        else:
            #print(type(s[0]))
            #print(type(int(s[1])))
            a.refil(s[0], int(s[1]))
    
__init__()    

